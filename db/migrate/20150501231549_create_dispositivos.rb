class CreateDispositivos < ActiveRecord::Migration
  def change
    create_table :dispositivos do |t|
      t.integer :condominio_id
      t.string :part_number
      t.string :nome
      t.string :fabricante
      t.string :zonas
      t.string :tipo_dispositivo
      t.integer :trigger_alarme
      t.integer :valor_limite_superior
      t.integer :valor_limite_inferior
      t.string :ip_address_dispositivo

      t.timestamps
    end
  end
end
