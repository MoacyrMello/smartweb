class CreateCondominios < ActiveRecord::Migration
  def change
    create_table :condominios do |t|
      t.string :nome
      t.string :lat
      t.string :long
      t.string :endereco
      t.string :bairro
      t.string :cidade
      t.string :estado
      t.string :cep
      t.string :cnpj
      t.string :numero_contrato_atual
      t.string :website

      t.timestamps
    end
  end
end
