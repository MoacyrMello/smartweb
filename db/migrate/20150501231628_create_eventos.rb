class CreateEventos < ActiveRecord::Migration
  def change
    create_table :eventos do |t|
      t.integer :dispositivo_id
      t.string :part_number
      t.string :tipo_evento
      t.integer :valor_registrado
      t.string :mensagem_evento
      t.string :data
      t.string :hora
      t.string :ip_address_registrado

      t.timestamps
    end
  end
end
