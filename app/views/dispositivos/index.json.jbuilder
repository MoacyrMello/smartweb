json.array!(@dispositivos) do |dispositivo|
  json.extract! dispositivo, :id, :condominio_id, :part_number, :nome, :fabricante, :zonas, :tipo_dispositivo, :trigger_alarme, :valor_limite_superior, :valor_limite_inferior, :ip_address_dispositivo
  json.url dispositivo_url(dispositivo, format: :json)
end
