json.array!(@condominios) do |condominio|
  json.extract! condominio, :id, :nome, :lat, :long, :endereco, :bairro, :cidade, :estado, :cep, :cnpj, :numero_contrato_atual, :website
  json.url condominio_url(condominio, format: :json)
end
