json.array!(@eventos) do |evento|
  json.extract! evento, :id, :dispositivo_id, :part_number, :tipo_evento, :valor_registrado, :mensagem_evento, :data, :hora, :ip_address_registrado
  json.url evento_url(evento, format: :json)
end
