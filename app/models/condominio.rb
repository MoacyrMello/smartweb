class Condominio < ActiveRecord::Base
    has_many :dispositivos, dependent: :destroy
    validates_presence_of :nome
end
