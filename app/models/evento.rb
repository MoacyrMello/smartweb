class Evento < ActiveRecord::Base
    belongs_to :dispositivo
    validates_presence_of :mensagem_evento
end
