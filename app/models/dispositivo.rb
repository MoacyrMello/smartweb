class Dispositivo < ActiveRecord::Base
    belongs_to :condominio
    has_many :eventos, dependent: :destroy
    validates_presence_of :nome
end
