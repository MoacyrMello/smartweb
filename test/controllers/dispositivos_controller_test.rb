require 'test_helper'

class DispositivosControllerTest < ActionController::TestCase
  setup do
    @dispositivo = dispositivos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dispositivos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dispositivo" do
    assert_difference('Dispositivo.count') do
      post :create, dispositivo: { condominio_id: @dispositivo.condominio_id, fabricante: @dispositivo.fabricante, ip_address_dispositivo: @dispositivo.ip_address_dispositivo, nome: @dispositivo.nome, part_number: @dispositivo.part_number, tipo_dispositivo: @dispositivo.tipo_dispositivo, trigger_alarme: @dispositivo.trigger_alarme, valor_limite_inferior: @dispositivo.valor_limite_inferior, valor_limite_superior: @dispositivo.valor_limite_superior, zonas: @dispositivo.zonas }
    end

    assert_redirected_to dispositivo_path(assigns(:dispositivo))
  end

  test "should show dispositivo" do
    get :show, id: @dispositivo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dispositivo
    assert_response :success
  end

  test "should update dispositivo" do
    patch :update, id: @dispositivo, dispositivo: { condominio_id: @dispositivo.condominio_id, fabricante: @dispositivo.fabricante, ip_address_dispositivo: @dispositivo.ip_address_dispositivo, nome: @dispositivo.nome, part_number: @dispositivo.part_number, tipo_dispositivo: @dispositivo.tipo_dispositivo, trigger_alarme: @dispositivo.trigger_alarme, valor_limite_inferior: @dispositivo.valor_limite_inferior, valor_limite_superior: @dispositivo.valor_limite_superior, zonas: @dispositivo.zonas }
    assert_redirected_to dispositivo_path(assigns(:dispositivo))
  end

  test "should destroy dispositivo" do
    assert_difference('Dispositivo.count', -1) do
      delete :destroy, id: @dispositivo
    end

    assert_redirected_to dispositivos_path
  end
end
